﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodeFirstAutomaticMigration.Models
{
    public class Worker
    {
        [Key]
        public int WorkerID { get; set; }
        [Column("Eesnimi")]
        [Required] 
        [StringLength(30, ErrorMessage = "Väli {0} võib sisaldada maksimaalselt {1} tähemärki.")]
        public string Name { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "Väli {0} võib sisaldada maksimaalselt {1} tähemärki ja minimaalselt {2} tähemärki.", MinimumLength = 2)]
        public string Surname { get; set; }
        [DisplayFormat(DataFormatString = "{0}€", ApplyFormatInEditMode = false)]
        public decimal Salary { get; set; }
        [Display(Name = "Bonus Percent")]
        [DisplayFormat(DataFormatString = "{0}%", ApplyFormatInEditMode = false)]
        [Range(0,20,ErrorMessage = "Väli {0} võimaldab sisestada väärtusi vahemikus {1} ja {2}")]
        public int BonusPercent { get; set; }
        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Range(typeof(DateTime),"1/1/1900", "1/1/2010", ErrorMessage = "Väli {0} võimaldab sisestada väärtusi vahemikus {1:d} ja {2:d}.")]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Start Time")]
        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Time)]
        public DateTime StartTime { get; set; }
        //[ForeignKey("BuildingID")]
        public int BuildingID { get; set; }
        public virtual Building Building { get; set; }

        [NotMapped]
        public int Age { get; set; }
    }
}