﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodeFirstAutomaticMigration.Models
{
    [Table("CompanyBuildings")]
    public class Building
    {
        [Key]
        public int BuildingID { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Worker> Workers { get; set; }
    }
}