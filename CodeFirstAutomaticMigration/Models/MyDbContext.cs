﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodeFirstAutomaticMigration.Models
{
    public class MyDbContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public MyDbContext() : base("name=MyDbContext")
        {
        }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Student> Students { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Department> Departments { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Employee> Employees { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Author> Authors { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Book> Books { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.AuthorToBook> AuthorsToBooks { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Building> Buildings { get; set; }

        public System.Data.Entity.DbSet<CodeFirstAutomaticMigration.Models.Worker> Workers { get; set; }
    }
}
