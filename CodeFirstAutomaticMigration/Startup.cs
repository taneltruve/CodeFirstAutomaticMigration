﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CodeFirstAutomaticMigration.Startup))]
namespace CodeFirstAutomaticMigration
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
